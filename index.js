const express = require('express')
const dotenv = require('dotenv')
const morgan = require('morgan')
const bodyParser = require('body-parser')
const mongoose = require('mongoose')

const app = express();

dotenv.config()
const PORT = process.env.APP_PORT
const DB_URL = `mongodb://${process.env.DATABASE_USERNAME}:${process.env.DATABASE_PASSWORD}@${process.env.DATABASE_HOST}:${process.env.DATABASE_PORT}/${process.env.DATABASE_NAME}`;
const db = mongoose.connection;
mongoose.connect(DB_URL, { useNewUrlParser: true }).then(() => console.log('DB Connected!'));
db.on('error', (err) => {
    console.log('DB connection error:', err.message);
})

app.use(morgan("dev"))
app.use(bodyParser.json())

const apiRoutes = require("./src/router");
app.use("/api", apiRoutes);

app.listen(PORT, () => {console.log("Server started on http://localhost:"+PORT)})

module.exports = app;