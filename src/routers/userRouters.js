const express = require('express'),
    router = express.Router()
const  UserController = require('../controllers/UserControllers')

router.post('/register',
    (req, res) => {
        UserController.register(req, res)
            .then(user => {
                if (user.err) {
                    res.json({err: user.err})
                } else {
                    res.json({ user: user, code: 200 })
                }
            })
        .catch(error => {
            return res.json({err: error})
        });
    }
);

router.post('/login',
    (req, res) => {
        UserController.login(req, res)
            .then(token => {
               res.json({token: token, code: 200})
            })
        .catch(error => {
            return res.json({err: error})
        });
    }
);



module.exports = router;
