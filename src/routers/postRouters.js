const express = require('express'),
    router = express.Router()
const  PostController = require('../controllers/PostControllers')

// router.use()

router.post('/new',
    (req, res) => {
        PostController.register(req, res)
            .then(user => {
                if (user.err) {
                    res.json({err: user.err})
                } else {
                    res.json({ user: user, code: 200 })
                }
            })
        .catch(error => {
            return res.json({err: error})
        });
    }
);

router.post('/detail/:id',
    (req, res) => {
        PostController.login(req, res)
            .then(token => {
               res.json({token: token, code: 200})
            })
        .catch(error => {
            return res.json({err: error})
        });
    }
);



module.exports = router;