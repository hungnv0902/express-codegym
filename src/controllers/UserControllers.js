const User = require('../models/UserModels')
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const UserController = function() {};

UserController.register = async (req, res) => {
    try {
        const user = await User.findOne({ email: req.body.email });
        if (!user) {
            const passwordHash = await bcrypt.hash(req.body.password, 10);
            let userData = {
                email: req.body.email,
                username: req.body.username,
                password: passwordHash,
                role: "customer"
            }
            const newUser = await User.create(userData);
            return newUser;
        } else {
            return { err: 'Email has been used' };
        }
    } catch (err) {
        return err
    }
}

UserController.login = async (req, res) => {
    try {
        const user = await User.findOne({ username: req.body.username });
        if (user) {
            const comparePass = await bcrypt.compare(req.body.password, user.password);
            if(!comparePass) {
                return Promise.reject({
					code: 404,
					message: "PASSWORD_NOT_VALID",
				});   
            }
            let payload = {
                user_id: user["id"],
			    email: user["email"],
                username: user["username"],
                role: user.role,
            }

            const token = jwt.sign(payload, 'hungnv', {
                expiresIn: 36000,
            });
            return token;
        } else {
            return { err: 'Email has been used' };
        }
    } catch (err) {
        return err
    }
}

module.exports = UserController;