"use strict";

const jwt = require("jsonwebtoken"),
	config = require("config");

function check_authen(req, res, next) {
	try {
		let accessToken =
		  req.headers[AppConstant.Authorization] ||
		  req.headers[AppConstant.XaccessToken];
		if (accessToken.startsWith('Bearer ')) {
		  accessToken = accessToken.slice(7, accessToken.length);
		}
		if (accessToken) {
		  jwt.verify(
			accessToken,
			AppConstant.secret_key_access_token,
			(err, decoded) => {
			  if (err) {
				return res.status(HttpStatus.UNAUTHORIZED).json({
				  message: err.message,
				  status: 401,
				});
			  } else {
				req.decoded = decoded;
				next();
			  }
			},
		  );
		} else {
		  return res.status(HttpStatus.UNAUTHORIZED).json({
			message: 'No token provided.',
			status: 401,
		  });
		}
	  } catch (err) {
		return res.status(HttpStatus.UNAUTHORIZED).json({
		  message: err.message,
		  status: 401,
		});
	  }
}

module.exports = check_authen;
