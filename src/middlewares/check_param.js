"use strict";

function validate_query_param(req, res, next) {
	// route middleware to validate limit or page
	let start = parseInt(req.query["_start"]),
		end = parseInt(req.query["_end"]);
	if (start >= 0 && end > 0) {
		req.query["limit"] = end - start;
		req.query["offset"] = start;
	} else {
		req.query["limit"] = 10;
		req.query["offset"] = 0;
	}
	next();
}

module.exports = validate_query_param;
