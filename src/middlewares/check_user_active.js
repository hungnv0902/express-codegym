var db = require("../database/schemas");

var checkUserActive = async (req, res, next) => {
	// Get user id from request decoded
	let userId = req.decoded["user_id"];
	let user = await db.User.findOne({ _id: userId }, "active");
	if (!user)
		return res.status(404).jsend.fail({
			status: "fail",
			code: 404,
			data: "USER_IS_NOT_ACTIVE",
		});
	else if (!user["active"])
		return res.status(403).jsend.fail({
			status: "fail",
			code: 403,
			data: "USER_IS_NOT_ACTIVE",
		});
	next();
};

module.exports = checkUserActive;
