"use strict";
var moment = require("moment");
var dateRegexFormat = /([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))/;

function checkDateFormat(dateStr, res) {
	if (dateStr && !dateRegexFormat.test(dateStr))
		return res.status(400).jsend.fail({
			status: "fail",
			code: 400,
			data: "Wrong date format",
		});
}

function convertDate(dateStr, type) {
	if (type == "start")
		return new Date(
			moment(dateStr)
				.set("hour", 0)
				.set("minute", 0)
				.set("second", 0)
		);
	else
		return new Date(
			moment(dateStr)
				.set("hour", 23)
				.set("minute", 59)
				.set("second", 59)
		);
}

function convert_datetime(req, res, next) {
	// route middleware convert date time
	let startDate = req.body["start_date"],
		endDate = req.body["end_date"],
		activeStart = req.body["active_start"],
		activeEnd = req.body["active_end"],
		expirationDate = req.body["expiration_date"];
	let arr = [];
	if (startDate) arr.push(startDate);
	if (endDate) arr.push(endDate);
	if (activeStart) arr.push(activeStart);
	if (activeEnd) arr.push(activeEnd);
	if (expirationDate) arr.push(expirationDate);
	arr.forEach(item => checkDateFormat(item));
	let condCreatedAt = {};
	if (startDate) {
		startDate = convertDate(startDate, "start");
		condCreatedAt = { $gte: startDate };
		req.body["start_date"] = startDate;
	}
	if (endDate) {
		endDate = convertDate(endDate, "end");
		condCreatedAt = { ...condCreatedAt, $lte: endDate };
		req.body["end_date"] = endDate;
	}
	if (Object.keys(condCreatedAt).length)
		req.body["created_at"] = condCreatedAt;
	// Check active_start end active_end for CMS list user
	if (activeStart) {
		activeStart = convertDate(activeStart, "start");
		req.body["active_start"] = new Date(activeStart);
	}
	if (activeEnd) {
		activeEnd = convertDate(activeEnd, "end");
		req.body["active_end"] = new Date(activeEnd);
	}
	if (!activeStart && activeEnd) {
		activeStart = convertDate("2020-01-01", "start");
		req.body["active_start"] = new Date(activeStart);
	}
	if (!activeEnd && activeStart) {
		activeEnd = convertDate(moment().format("YYYY-MM-DD"), "end");
		req.body["active_end"] = new Date(activeEnd);
	}
	next();
}

module.exports = convert_datetime;
