Tạo thư mục chứa dự án (mkdir node-express-codegym).
cd node-express-codegym
npm init
npm install express
Tạo file index.js có nội dung sau
const express = require('express')
const app = express()
const PORT = 8797
app.use('/', (req, res) => {
    res.json({"mess": "Hello Would!"})
})


app.listen(PORT, () => {console.log("Server started on http://localhost:"+PORT)})


module.exports = app;

-	Vào postman và test lại: node index.js

Cài đặt nodemon để server tự chạy lại khi có sự thay đổi: npm install nodemon
Xem lại file package.json xem có nodemon hay chưa.
Cài các package cần dùng: Các package cần dùng:
body-parser (parse các request tới server)
express (làm cho ứng dụng chạy)
nodemon (restart khi có thay đổi xảy ra)
mongoose (mô hình hóa object data để đơn giản hóa các tương tác với MongoDB)
bcrypt (hashing và salting passwords)
express session (xử lý sessions)
connect-mongo (lưu trữ session trong MongoDB)
dotenv (sử dụng .env)
morgan
...:~/MyNodeProject$ npm i body-parser mongoose bcrypt express session connect-mongo dotenv morgan
connect mongodb
Tạo 1 db và user, pass trên mongodb
linh truy cập db có dạng mongodb://<dbuser>:<dbpassword>@ds111549.mlab.com:11549/mynodeproject_db
Tạo 1 file .env với nội dung sau
DATABASE_HOST = '127.0.0.1'
DATABASE_PORT = '27017'
DATABASE_USERNAME = 'hung1'
DATABASE_PASSWORD = '123456'
DATABASE_NAME = 'codegym'
NODE_ENV = 'dev'
APP_HOST = '0.0.0.0'
APP_PORT = '5000'

Sửa file inđex.js 
const express = require('express')
const dotenv = require('dotenv')
const morgan = require('morgan')
const bodyParser = require('body-parser')
const mongoose = require('mongoose')

const app = express();

dotenv.config()
const PORT = process.env.APP_PORT
const DB_URL = `mongodb://${process.env.DATABASE_USERNAME}:${process.env.DATABASE_PASSWORD}@${process.env.DATABASE_HOST}:${process.env.DATABASE_PORT}/${process.env.DATABASE_NAME}`;
const db = mongoose.connection;
mongoose.connect(DB_URL, { useNewUrlParser: true }).then(() => console.log('DB Connected!'));
db.on('error', (err) => {
    console.log('DB connection error:', err.message);
})

app.use(morgan("dev"))
app.use(bodyParser.json())

const apiRoutes = require("./src/router");
app.use("/api", apiRoutes);

app.listen(PORT, () => {console.log("Server started on http://localhost:"+PORT)})

module.exports = app;

chạy npm run start:dev
Tạo file src/models/UserModels.js
Tạo file src/controllers/UserControllers.js
Tạo file src/routes/userRouters.js
 -	Tạo file src/router.js

Thử đăng ký tài khoản với postman
